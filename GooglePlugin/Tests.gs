/**
 * Kind of non regression function - run this one to see if you break anything
 */
function tests(){
  var aBtcUsd = 15000;
  var aEthUsd = 800; //:(
  var aXrpUsd = 1;
  if(false){
    //homemade function helper test
    Logger.log("Starting tests for isKeyValidForProvider function");
    Logger.log(isKeyValidForProvider("ETHUSD", "gemini") + " should output true");
    Logger.log(isKeyValidForProvider("XRPUSD", "gemini") + " should output false");
    Logger.log(isKeyValidForProvider("XRPUSD", "gemdax") + " should output false");
    Logger.log(GET_VERSION() + " should output " + aVersion);
    // ?? Logger.log(getProvider("gemini")===aGemini + " should output true");
    Logger.log(getProvider("gemini").name + " should output gemini");
  }
  //Entry point test
  if(true){
    Logger.log("Starting tests for GET_CRYPTO function");
    Logger.log(GET_CRYPTO("gemini","BTCUSD") + " should output around:" + aBtcUsd);
    Logger.log(GET_CRYPTO("gemini","ETHUSD") + " should output around:" + aEthUsd);
    Logger.log(GET_CRYPTO("kraken","BTCUSD") + " should output around:" + aBtcUsd);
    Logger.log(GET_CRYPTO("kraken","ETHUSD") + " should output around:" + aEthUsd);
    Logger.log(GET_CRYPTO("kraken","XRPUSD") + " should output around:" + aXrpUsd);
    Logger.log(GET_CRYPTO("gdax","BTCUSD") + " should output around:" + aBtcUsd);
    Logger.log(GET_CRYPTO("gdax","ETHUSD") + " should output around:" + aEthUsd);
    Logger.log(GET_CRYPTO("bitfinex","BTCUSD") + " should output around:" + aBtcUsd);
    Logger.log(GET_CRYPTO("bitfinex","ETHUSD") + " should output around:" + aEthUsd);
    Logger.log(GET_CRYPTO("bitfinex","XRPUSD") + " should output around:" + aXrpUsd);
    Logger.log(GET_CRYPTO("poloniex","BTCUSD") + " should output around:" + aBtcUsd);
    Logger.log(GET_CRYPTO("bittrex","BTCUSD") + " should output around:" + aBtcUsd);
    Logger.log(GET_CRYPTO("bittrex","ETHUSD") + " should output around:" + aEthUsd);
    //Wrong provider
    try {
      GET_CRYPTO("toto","BTCUSD");
      Logger.log("If you see this log there is an error in the GET_CRYPTO function when the provider is unknow");
    } catch(e) {
      Logger.log("You should see this log");
    }
    //Wrong KeyPair TEST
    try {
      GET_CRYPTO("gemini","TEST");
      Logger.log("If you see this log there is an error in the GET_CRYPTO function when the keypair is wrong");
    } catch(e) {
      Logger.log("You should see this log");
    }
    //Ensure that we retrun a number and not a string
    Logger.log("Test if we return really a number and not a string:" + isNaN(GET_CRYPTO("gemini","BTCUSD")));
  }
}